<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
         <form action="" method="post">
            <div>
                <label for="dato1">Numero 1</label>
                <input type="number" name="dato1" id="dato1">
            </div>
            <br>
            <div>
                <label for="dato2">Numero 2</label>
                <input type="number" name="dato2" id="dato2">
            </div>
            <button type="submit">Enviar</button>
        </form>
        
        <?php
        if($_POST){
            $numero1 = $_POST["dato1"];
            $numero2 = $_POST["dato2"];
            
            $suma = $numero1 + $numero2;
            echo "El resultado de la suma de los numeros $numero1 y $numero2 es: $suma";
        }
        ?>
    </body>
</html>
