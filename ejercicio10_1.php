<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $radio = $_POST["radio"];
            $PI = pi();
            
            $lon = 2*$PI*$radio;
            $area = $PI* pow($radio, 2);
            $volu = 4/3 *$PI* pow($radio, 3);
            
            echo "El circulo con radio: $radio<br>
                tiene una longitud de $lon,<br>
                un area de $area,<br>
                y un volumen de $volu";
         
        ?>
    </body>
</html>
